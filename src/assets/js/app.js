import 'babel-polyfill'
import $ from 'jquery';
import whatInput from 'what-input'
//
import googleMaps from './lib/google-maps'
import darkStyle from './lib/google-maps-styles/dark'
import greyStyle from './lib/google-maps-styles/grey'
import carousels from './lib/carousels'
import hamburgerClick from './lib/hamburger-click'
import slideToggle from './lib/slide-toggle'
import menuClose from './lib/mobile-menu-close'
import googleAnalytics from './google-analytics'
import hotjar from './lib/hot-jar'
import conditionalForm from './conditional-form'
import vobOpen from './lib/vob-open'
import vobRecaptcha from './lib/vob-recaptcha'
import stickyCTA from './lib/sticky-cta'
// import liveHelpNow from './live-help-now'

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

// ZURB Foundation
$(document).foundation();

// Sticky CTA
stickyCTA()

// Live Help Now
// liveHelpNow()

// Google Analytics
googleAnalytics()

// Google Maps
const stylePicker = style => (style === 'grey' ? greyStyle : null)
googleMaps(stylePicker)

// Carousel
carousels()

//Hamburger Click
hamburgerClick()

//Menu Close
menuClose()